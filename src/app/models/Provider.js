import Sequelize, { Model } from 'sequelize';

class Provider extends Model {
  static init(sequelize) {
    super.init(
      {
        description: Sequelize.TEXT,
        occupation: Sequelize.STRING,
        price_hour: Sequelize.DECIMAL,
        initial_hour: Sequelize.TIME,
        final_hour: Sequelize.TIME,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id' });
    this.belongsTo(models.Category, {
      foreignKey: 'category_id',
      as: 'category',
    });
  }
}

export default Provider;
