// import { startOfDay, endOfDay, parseISO } from 'date-fns';
// import { Op } from 'sequelize';
import User from '../models/User';
import Provider from '../models/Provider';
import File from '../models/File';

import Appointment from '../models/Appointment';

class ScheduleController {
  async index(req, res) {
    const { provider_id } = req.params;

    const provider = await Provider.findOne({
      where: { id: provider_id },
    });

    if (!provider) {
      return res.status(400).json({ error: 'User is not a provider' });
    }

    // const { date } = req.query;
    // const parseDate = parseISO(date);

    const appointments = await Appointment.findAll({
      where: {
        provider_id: provider.id,
        canceled_at: null,
        // date: {
        //  [Op.between]: [startOfDay(parseDate), endOfDay(parseDate)],
        // },
      },
      order: ['date'],
      include: {
        model: User,
        include: {
          model: File,
        },
      },
    });

    return res.json(appointments);
  }
}

export default new ScheduleController();
