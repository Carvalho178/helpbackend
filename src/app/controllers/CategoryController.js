import Category from '../models/Category';

class CategoryController {
  async index(req, res) {
    const categories = await Category.findAll({
      attributes: ['id', 'name', 'description'],
    });
    return res.status(200).json(categories);
  }
}
export default new CategoryController();
