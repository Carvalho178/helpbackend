import State from '../models/State';

class StateController {
  async index(req, res) {
    const states = await State.findAll({
      attributes: ['id', 'name', 'uf'],
    });
    return res.status(200).json(states);
  }
}
export default new StateController();
