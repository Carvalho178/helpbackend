import * as Yup from 'yup';
import Provider from '../models/Provider';
import User from '../models/User';
import File from '../models/File';

class ProviderController {
  async store(req, res) {
    const schema = Yup.object().shape({
      user_id: Yup.number().required(),
      category_id: Yup.number().required(),
      description: Yup.string().required(),
      occupation: Yup.string().required(),
      price_hour: Yup.number().required(),
      initial_hour: Yup.string().required(),
      final_hour: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const providerExists = await Provider.findOne({
      where: { user_id: req.body.user_id },
    });

    if (providerExists) {
      return res.status(400).json({ error: 'Provider already exists' });
    }

    const provider = await Provider.create(req.body);

    return res.json(provider);
  }

  async index(req, res) {
    const providers = await Provider.findAll({
      include: {
        model: User,
        include: {
          model: File,
        },
      },
    });
    return res.json(providers);
  }

  async getOne(req, res) {
    const { id } = req.params;

    const provider = await Provider.findByPk(id, {
      include: {
        model: User,
        include: {
          model: File,
        },
      },
    });

    return res.json(provider);
  }

  async getOneByUser(req, res) {
    const { id } = req.params;

    const provider = await Provider.findOne({
      where: { user_id: id },
      include: {
        model: User,
        include: {
          model: File,
        },
      },
    });

    return res.json(provider);
  }
}

export default new ProviderController();
