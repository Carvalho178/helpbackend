import * as Yup from 'yup';
import { startOfHour, parseISO, isBefore, subHours } from 'date-fns';
import { Op } from 'sequelize';
import Appointment from '../models/Appointment';
import User from '../models/User';
import Provider from '../models/Provider';
import File from '../models/File';

class AppointmentController {
  async index(req, res) {
    const { user_id } = req.params;
    const { page = 1 } = req.query;

    const appointments = await Appointment.findAll({
      where: {
        [Op.or]: {
          user_id,
        },
        canceled_at: null,
      },
      order: ['date'],
      limit: 20,
      offset: (page - 1) * 20,
      include: [
        {
          model: Provider,
          include: {
            model: User,
            include: {
              model: File,
            },
          },
        },
      ],
    });
    return res.status(200).json(appointments);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      provider_id: Yup.number().required(),
      user_id: Yup.number().required(),
      date: Yup.date().required(),
      address: Yup.string().required(),
      city: Yup.string().required(),
      complement: Yup.string().required(),
      cep: Yup.string().required(),
      state_id: Yup.number().required(),
    });
    console.log(req.body);
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const {
      user_id,
      provider_id,
      date,
      address,
      city,
      complement,
      cep,
      state_id,
    } = req.body;

    const providerExists = await Provider.findOne({
      where: { id: provider_id },
    });

    if (!providerExists) {
      return res.status(401).json({ error: 'Provider not exists' });
    }

    const userExists = await User.findOne({
      where: { id: user_id },
    });

    if (!userExists) {
      return res.status(401).json({ error: 'User not exists' });
    }

    const hourStart = startOfHour(parseISO(date));

    if (isBefore(hourStart, new Date())) {
      return res.status(400).json({ error: 'Past dates are not permitted' });
    }

    const checkAvailability = await Appointment.findOne({
      where: {
        provider_id,
        canceled_at: null,
        date: hourStart,
      },
    });

    if (checkAvailability) {
      return res
        .status(400)
        .json({ error: 'Appointment date is not available' });
    }

    const appointment = await Appointment.create({
      provider_id,
      user_id,
      date: hourStart,
      address,
      city,
      complement,
      cep,
      state_id,
    });

    return res.status(200).json(appointment);
  }

  async delete(req, res) {
    const appointment = await Appointment.findByPk(req.params.appointment_id);

    const dateWithSub = subHours(appointment.date, 2);

    if (isBefore(dateWithSub, new Date())) {
      return res.status(401).json({
        error: 'You can only cancel appointments 2 hours in advance',
      });
    }

    appointment.canceled_at = new Date();

    await appointment.save();

    return res.json(appointment);
  }
}

export default new AppointmentController();
