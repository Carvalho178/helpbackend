import { Router } from 'express';
import cors from 'cors';
import multer from 'multer';
import multerConfig from './config/multer';
// import authMiddleware from './app/middlewares/auth';

import UserController from './app/controllers/UserController';
import StateController from './app/controllers/StateController';
import CategoryController from './app/controllers/CategoryController';
import ProviderController from './app/controllers/ProviderController';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import AppointmentController from './app/controllers/AppointmentController';
import ScheduleController from './app/controllers/ScheduleController';

const routes = new Router();
const upload = multer(multerConfig);

routes.use(cors());

routes.post('/sessions', SessionController.store);
routes.post('/users', UserController.store);
routes.get('/states', StateController.index);

routes.post('/files', upload.single('file'), FileController.store);

// auth
// routes.use(authMiddleware);

routes.post('/providers', ProviderController.store);
routes.get('/providers', ProviderController.index);
routes.get('/providers/:id', ProviderController.getOne);
routes.get('/providers/user/:id', ProviderController.getOneByUser);
routes.get('/categories', CategoryController.index);
routes.post('/appointments', AppointmentController.store);
routes.get('/appointments/:user_id', AppointmentController.index);
routes.delete('/appointments/:appointment_id', AppointmentController.delete);

routes.get('/schedule/:provider_id', ScheduleController.index);

export default routes;
