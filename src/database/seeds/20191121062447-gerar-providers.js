module.exports = {
  up: QueryInterface => {
    return QueryInterface.bulkInsert(
      'providers',
      [
        {
          user_id: 2,
          category_id: 1,
          description:
            'Realizo formatação e manutenção de computadores, Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. Quem manda na minha terra sou euzis! In elementis mé pra quem é amistosis quis leo. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.',
          occupation: 'Técnico de Informática',
          price_hour: '30.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 3,
          category_id: 2,
          description:
            'Mussum Ipsum, cacilds vidis litro abertis. Casamentiss faiz malandris se pirulitá. Quem manda na minha terra sou euzis! Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Praesent malesuada urna nisi, quis volutpat erat hendrerit non. Nam vulputate dapibus.',
          occupation: 'Programador',
          price_hour: '40.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 4,
          category_id: 3,
          description:
            'Cevadis im ampola pa arma uma pindureta. Atirei o pau no gatis, per gatis num morreus. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
          occupation: 'Mecanico',
          price_hour: '60.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 5,
          category_id: 4,
          description:
            'Delegadis gente finis, bibendum egestas augue arcu ut est. Viva Forevis aptent taciti sociosqu ad litora torquent. Quem num gosta di mé, boa gentis num é. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo!',
          occupation: 'Mestre de obra',
          price_hour: '80.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 6,
          category_id: 5,
          description:
            'INOFRMAÇÃO, ENERGIA, FREQUÊNCIA E VIBRAÇÃO: O coaching quântico busca alinhar corpo, mente e espírito através do autoconhecimento e autoconsciência. É caracterizado como um processo holístico que se baseia em outros métodos alternativos, como Medicina Chinesa, Meditação, Cura Prânica, Shiatsu e Reiki. Os profissionais especializados em coaching quântico geralmente também praticam esses meios.',
          occupation: 'Coach Quântico',
          price_hour: '340.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 7,
          category_id: 9,
          description:
            'Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. Aenean aliquam molestie leo, vitae iaculis nisl. Posuere libero varius. Nullam a nisl ut ante blandit hendrerit. Aenean sit amet nisi. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.',
          occupation: 'Técnico em Enformagem',
          price_hour: '100.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 8,
          category_id: 7,
          description:
            'Diuretics paradis num copo é motivis de denguis. Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose. Mé faiz elementum girarzis, nisi eros vermeio. Paisis, filhis, espiritis santis.',
          occupation: 'Professor Particular',
          price_hour: '80.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 9,
          category_id: 7,
          description:
            'A ordem dos tratores não altera o pão duris. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis. Suco de cevadiss deixa as pessoas mais interessantis. Copo furadis é disculpa de bebadis, arcu quam euismod magna.',
          occupation: 'Professor Particular',
          price_hour: '20.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          user_id: 10,
          category_id: 7,
          description:
            'Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.',
          occupation: 'Professor Particular',
          price_hour: '40.00',
          initial_hour: '08:00',
          final_hour: '18:00',
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
  },

  down: () => {},
};
