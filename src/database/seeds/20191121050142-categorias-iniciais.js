module.exports = {
  up: QueryInterface => {
    return QueryInterface.bulkInsert(
      'categories',
      [
        {
          name: 'Assistência Técnica',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Design e Tecnologia',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Auto',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Construção/Manutenção/Reforma',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Consultoria',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Evento',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Aulas Particulares',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Moda e Beleza',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Saúde',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Saúde Animal',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Serviços Domésticos',
          description: 'Categoria legal - Teste Inicial',
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
  },

  down: () => {},
};
