import Sequelize from 'sequelize';

import User from '../app/models/User';
import State from '../app/models/State';
import File from '../app/models/File';
import Category from '../app/models/Category';
import Provider from '../app/models/Provider';
import Appointment from '../app/models/Appointment';

import databaseConfig from '../config/database';

const models = [User, State, File, Category, Provider, Appointment];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
