1) Primeiro deve criar o container do banco

> docker run --name database -e POSTGRES_PASSWORD=docker -p 5432:5432 -d postgres

2) Em seguida criar o database "help" dentro do banco

3) Entrar na pasta backend e executar os comandos:

> docker build -t help/backend .

> docker run --name help-backend -p 3333:3333 -d help/backend

4) Entrar no arquivo HelpFrontend e executar:

> docker build -t help/frontend .

> docker run --name help-frontend -p 3000:3000 -d help/frontend

5) acessar localhost:3000 e usar o projeto Help
